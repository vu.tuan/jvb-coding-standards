Tài liệu coding standard dưới đây được dựa trên các bản PHP Specification Request (PSR)
của nhóm [PHP Framework Interop Group (PHP FIG)](http://www.php-fig.org/)

Các bản gốc bằng tiếng Anh có thể được tham khảo tại repository của nhóm trên [github](https://github.com/php-fig/fig-standards)

- [PSR-1 Basic Coding Standard](./PSR-1.md) :  - Các quy tắc cơ bản liên trong việc coding (Việc đặt tên file, tên class, tên method, tên constant ...
- [PSR-2 Coding Style Guide](./PSR-2.md) :  - Coding Style Guide - Các quy tắc liên quan đến trình bày code (căn lề, dấu cách ...)
- [PSR-3 Logger Interface](./PSR-3.md) :  - Những quy tắc cơ bản khi viết interface cho một thư viện logging.
- [PSR-4 Autoloader](./PSR-4.md) :  - Những quy tắc lưu trữ file sẽ được load.
- [Others](./others.md)
- [Fix convention usephpcs](./phpcs.md)
