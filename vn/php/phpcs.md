### 1. Cài đặt php_codesniffer
```
composer require --dev squizlabs/php_codesniffer
```
### 2. Cài đặt extension PHP CS Fixer
```

```
### 3. Tạo file .git/hook/pre-commit trong folder code với nội dung
```
#!/bin/sh
 
PROJECT=`php -r "echo dirname(dirname(dirname(realpath('$0'))));"`
STAGED_FILES_CMD=`git diff --cached --name-only --diff-filter=ACMR HEAD | grep \\\\.php`
 
# Determine if a file list is passed
if [ "$#" -eq 1 ]
then
	oIFS=$IFS
	IFS='
	'
	SFILES="$1"
	IFS=$oIFS
fi
SFILES=${SFILES:-$STAGED_FILES_CMD}
 
echo "Checking PHP Lint..."
for FILE in $SFILES
do
	php -l -d display_errors=0 $PROJECT/$FILE
	if [ $? != 0 ]
	then
		echo "Fix the error before commit."
		exit 1
	fi
	FILES="$FILES $PROJECT/$FILE"
done
 
if [ "$FILES" != "" ]
then
	echo "Running Code Sniffer..."
	vendor/bin/phpcs --standard=PSR2 --encoding=utf-8 -n -p $FILES
	if [ $? != 0 ]
	then
		echo "Coding standards errors have been detected. Running phpcbf..."
		vendor/bin/phpcbf --standard=PSR2 --encoding=utf-8 -n -p $FILES
		git add $FILES
		echo "Running Code Sniffer again..."
		vendor/bin/phpcs --standard=PSR2 --encoding=utf-8 -n -p $FILES
		if [ $? != 0 ]
		then
			echo "Errors found not fixable automatically"
			exit 1
		fi
	fi
fi
 
exit $?
```
Chú ý: chmod +x .git/hooks/pre-commit
