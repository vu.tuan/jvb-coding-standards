## JVB's coding standard (Tổng hợp)

| Flow     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; |Tiếng Việt &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    |English  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  |
|:-------------------|-----------------------------------:|-------------------------------------:    |
| PHP       | [Link](./vn/php/README.md)    | [Link](./vn/php/README.md)    |
| Java      | [Link](./vn/java/README.md)   | [Link](./vn/java/README.md)   |
| HTML      | [Link](./vn/html/README.md)   | [Link](./vn/html/README.md)   |
| CSS       | [Link](./vn/html/README.md)   | [Link](./vn/html/README.md)   |
| Javascript| [Link](./vn/js/README.md)     | [Link](./vn/js/README.md)     |
| Ruby      | Todo... |    Todo... |
| Python    | Todo... |    Todo... |
| Git       | [Link](./vn/git/flow.md)      |  [Link](./vn/git/flow.md)     |
