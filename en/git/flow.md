## JVB Việt Nam Git flow

Flow tham khảo: [A successful Git branching model](http://nvie.com/posts/a-successful-git-branching-model/)

### Giả định
* Đã tạo Central Repository (Nguồn trung tâm) trên https://gitlab.hatoq.com）.
* Branch mặc định của Central Repository là `master`.
* Đã quyết định người review và người có quyền merge.

### Nguyên tắc
* Mỗi pull-request tương ứng với một ticket.
* Mỗi một pull-request không hạn chế số lượng commit
* Pull-request title phải đặt sao cho tương ứng với title của task với format `refs [Loại ticket] #[Số ticket] [Nội dung ticket]` （Ví dụ: `refs bug #1234 Sửa lỗi cache`）.
* Đối với commit title, trong trường hợp pull-request đó chỉ có 1 commit thì có thể đặt commit title tương tự như trên là `refs [Loại ticket] #[Số ticket] [Nội dung ticket]` （Ví dụ: `refs bug #1234 Sửa lỗi cache`）.\
  Tuy nhiên với trường hợp 1 ppull-request có chứa nhiêù commit thì cần phải ghi rõ trong nội dung commit title là trong commit đó xử lý đối ứng vấn đề gì.
    * Ví dụ:
        1. Pull-request title: `refs bug #1234 Sửa lỗi cache`
        2. Trong trường hợp pull-request có 2 commit thì nội dung commit title của 2 commit sẽ tương ứng như sau
            * `Tạo method thực hiện việc clear cache trong Model`
            * `Tại controller gọi method ở Model để thực hiện việc clear cache`
* Không được force push, trong trường hợp cần sử dụng force push thì cần có sự đồng thuận từ team.
* Tại môi trường local(trên máy lập trình viên), tuyệt đối không được thay đổi code khi ở branch master.Nhất định phải thao tác trên branch khởi tạo để làm task.

### Chuẩn bị
1. Chắc chắn đã được add vào repository.
2. Clone (tạo bản sao) ở môi trường local.Tại thời điểm này, 
    ```sh
    $ git clone [URL của Repository]
    ```
	
### Quy trình

1. Tạo branch để làm task từ branch master ở local. Tên branch là số ticket của task（Ví dụ: `task/1234`）.
    ```sh
    $ git checkout master # <--- Không cần thiết nếu đang ở trên branch master
    $ git checkout -b task/1234
    ```

2. Tiến hành làm task（Có thể commit bao nhiêu tùy ý）.
	 Trường hợp đã tạo nhiều commit trong quá trình làm task. trước khi push có thể dùng (hoặc không) rebase -i để hợp các commit lại thành 1 commit duy nhất.
    ```sh
    $ git rebase -i [Giá trị hash của commit trước commit đầu tiên trong quá trình làm task]
    ```
3. Push code lên origin.
    ```sh
    $ git push origin task/1234
    ```

4. Tại origin trên https://gitlab.hatoq.com branch `task/1234` đã được push lên hãy tạo Merge Requests.
	Tiến hành kiểm tra lại link URL của merge request đó có bị conflict không, nếu có phải resolve hết conflict.

5. Hãy gửi link URL của trang Merge Requests cho reviewer trên để tiến hành review code.

    5.1. Trong trường hợp reviewer có yêu cầu sửa chữa, hãy thực hiện các bước 2. 〜 4.
    5.2. Tiếp tục gửi lại URL cho reviewer để tiến hành việc review code.

6. Nếu người reviewer đồng ý với merge requests, người reviewer sẽ tiếp hành merge.
   
7. Quay trở lại 1.
